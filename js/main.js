const countries = "https://restcountries.com/v3.1/all";

function fetchData(url) {
  return fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      return new Error("404 Not Found");
    }
  });
}
let countryMap = new Map();
let code = new Map();

// // Fetching the data
fetchData(countries)
  .then((countriesArray) => {
    countriesArray.forEach((country) => {
      countryMap.set(country.name.common, country);
    });
    countriesArray.forEach((country) => {
      code.set(`${country.cca3}`, `${country.name.common}`);
    });

    return countriesArray;
  })
  .then((datastream) => {
    const select = document.getElementById("regions");
    select.addEventListener("change", filterByRegion);
    datastream.forEach((data) => {
      const markup = ` 
      <div class="card">
        <div class="upper-part">
          <img src="${data.flags.png}" />
        </div>
        <div class="lower-part">
          <div class="content">
            <h1>${data.name.common}</h1>
            <p>Population: ${data.population}</p>
            <p class='region'>Region: ${data.continents}</p>
            <p>Capital: ${data.capital}</p>
          </div>
        </div>
      </div>
      `;
      document.querySelector("#load").style.display = "None";
      document.querySelector(".data").insertAdjacentHTML("beforeend", markup);
    });
  })
  .then(() => {
    const elements = Array.from(document.getElementsByClassName("card"));

    elements.forEach((curr) => {
      curr.addEventListener("click", (cardEvent) => {
        let card = cardEvent.target;
        while (
          !card.querySelector(".upper-part") ||
          !card.querySelector(".lower-part")
        ) {
          card = card.parentElement;
        }
        const countryData = countryMap.get(
          card.querySelector("h1").textContent
        );
        const neighbours = countryData.borders
          ? countryData.borders.map((border) => code.get(border)).join(", ")
          : "No Borders";
        const markup = ` 
        <div class="popup" id="pop-up">
              <div class="left-part">
              <button class="close-button" onClick="btnClose()">&times;</button>
              <img src="${countryData.flags.png}"
              alt="flag" />
              </div>
              <div class="right-part">
              <h1>${countryData.name.common}</h1>
              <div class="country-data">
                      <div class="imp-data">
                          <p><span>Native Name:</span> ${
                            Object.values(countryData.name.nativeName)[0].common
                          }</p>
                          <p><span>Population:</span> ${
                            countryData.population
                          }</p>
                          <p><span>Region:</span> ${countryData.region}</p>
                          <p><span>Sub Region:</span> ${
                            countryData.subregion
                          }</p>
                          <p><span>Capital:</span> ${countryData.capital}</p>
                      </div>
                      <div class="other-data">
                          <p><span>Top Level Domain:</span> ${
                            countryData.tld
                          }</p>
                          <p><span>Currencies:</span> ${
                            Object.values(countryData.currencies)[0].name
                          }</p>
                          <p><span>Languages:</span> ${Object.values(
                            countryData.languages
                          )}</p>
                      </div>
                      <div class="border-counties">
                          <h3><span>Border Countries:</span> ${neighbours}</h3>
                          
                      </div>
                  </div>
              </div>
          </div>
        `;
        document.querySelector(".data").style.display = "none";
        document
          .querySelector(".popup-data")
          .insertAdjacentHTML("beforeend", markup);
      });
    });
  })
  .catch((err) => {
    const markup = `
    <div class="card">
        <div class="upper-part error">
                <img src="static/error.png" alt="earth" />
        </div>
        <div class="lower-part">
          <div class="content">
            <h1>OOPS! An Error Occured.</h1>
            <p>${err}</p>
          </div>
        </div>
   </div>
    `;
    document.querySelector("#load").style.display = "None";

    document.querySelector(".data").insertAdjacentHTML("beforeend", markup);
  });

function btnClose() {
  const removeElement = document.getElementById("pop-up");
  removeElement.remove();
  document.querySelector(".data").style.display = "flex";
}

function filterByRegion(event) {
  let current = event.target.value.toUpperCase();
  if (current !== "ALL") {
    let allCards = Array.from(document.querySelectorAll(".card"));
    allCards.forEach((card) => {
      let region = card
        .querySelector(".region")
        .textContent.slice(8)
        .toUpperCase();
      if (region.includes("AMERICA")) region = "AMERICA";
      console.log(current, region);
      if (region != current) {
        card.style.display = "none";
      } else {
        card.style.display = "flex";
      }
    });
  } else {
    let allCards = Array.from(document.querySelectorAll(".card"));
    allCards.forEach((card) => {
      card.style.display = "flex";
    });
  }
}
